# k8s-sensu

This repo is intended as a small functional example of using Sensu in Kubernetes (K8s).

## Usage

The services contained in this repository are designed to use the AWS LoadBalancer construct.  Therefore this may take some work to run outside of AWS (and/or EKS).  All resources can be brought up using `kubectl apply -f <resource_spec>`

## Dependency Order

0. The Kubernetes cluster must have an `ebs-gp2` storage class.
1. Make sure that the Sensu Docker image is built and available (`build.sh`)
2. Redis
    1. `redis-vol-persistentvolumeclaim.yaml`
    2. `redis-deployment.yaml`
    3. `redis-service.yaml`
3. Sensu API
    1. `api-deployment.yaml`
    2. `api-service.yaml`
4. Sensu Server
    1. `url-checks-configmap.yaml`
    2. `server-deployment.yaml`
5. Sensu Client
    1. `client-deployment.yaml`
6. Uchiwa Dashboard
    1. `uchiwa-deployment.yaml`
    2. `uchiwa-service.yaml`

## Anatomy

The Dockerfile uses the existing `sstarcher/sensu` image, but adds the http check.  The resulting image is used by the client, server, and API.

The `url-checks-configmap.yaml` check includes some basic examples.  It's worth noting that the `sstarcher/sensu` image doesn't work well with standalone checks, so these example http checks are run as a subscriber-based check.  Unless it's applied to a small subset of clients (which this example doesn't do), it won't scale well.  (Without restricting the pool of subscribers, it would basically DDoS the target.)

- sensu-server to orchestrate clients, checks, events, etc.
- sensu-client to run checks (which are fed to it from the server)
- Uchiwa - The web UI to interact with Sensu
- sensu-api allows Uchiwa or other outside entities to interact with Sensu
- Redis as a transport to track events and state
